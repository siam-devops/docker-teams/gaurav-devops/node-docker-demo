/**
 * This is the starting point for the application.
 * 
 * @file AppConfig.js
 * @fileoverview Contains basic configuration details of the app.
 * @author Gaurav Kanted <gaurav@siamcomputing.com>
 * @requires dotenv 
 * */
require('dotenv').config({ path: './Config/.env' })

module.exports = {
    PORT: process.env.PORT,
    MONGODB_CONNECTION: process.env.MONGODBHOST
}