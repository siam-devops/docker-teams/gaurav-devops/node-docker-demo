FROM node:14-alpine
RUN mkdir /home/node/code
WORKDIR /home/node/code
COPY . /home/node/code
RUN npm install nodemon -g
RUN npm install
ENTRYPOINT ["nodemon", "/home/node/code/app.js"]